# LPaaS Docker image

## Build

The build of the Dockerfile requires the presence of an `lpaas.war` file inside default gradle output folder (`build/libs/`) of a subfolder named `lpaas/`;
to decouple Docker image development from main codebase, these folders are not include in this repository on purpose.

To build the Docker image, this is an example:

```bash
git clone https://gitlab.com/pika-lab/lpaas/lpaas-ws.git lpaas
cd ./lpaas
gradle war
cd ..
docker build .
```

## CI

The CI requires the following environment variables to work correctly:
  
  - `CI_REGISTRY`: the Docker registy
  - `CI_REGISTRY_USER`: the Docker registry username
  - `CI_REGISTRY_PASSWORD`: the password for the Docker registry user
  - `CI_REGISTRY_IMAGE`: the name of the Docker image (ie. "lpaas/lpaas")
